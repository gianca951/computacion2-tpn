"""
Realice un programa que genere dos procesos. El proceso hijo1 enviará la señal
SIGUSR1 al proceso padre y mostrará la cadena "ping" cada 5 segundos. El proceso
padre, al recibir la señal SIGUSR1 enviará esta misma señal al proceso hijo2. El
proceso hijo2, al recibir dicha señal mostrará la cadena "pong" por pantalla.

"""
import signal, os
from time import sleep

def handler(s,f):
    print("...Señal recibida por el padre...")

def handler2(s,f):
    print("...Enviando Señal...")

def hijo1():
    while True:
        print("\nSoy proceso hijo 1 con pid %d : 'PING'" % os.getpid())
        os.kill(pidPadre, signal.SIGUSR1)
        sleep(5)
    os._exit(0)

def hijo2():
    while True:
        print("Soy proceso hijo 2 con pid %d: 'PONG'" % os.getpid())
        signal.pause()

def padre():
    pid2 = os.fork()
    if pid2 == 0:
        signal.signal(signal.SIGUSR1,handler2)
        hijo2()
    else:
        while True:
            os.kill(pid2, signal.SIGUSR1)
            signal.pause()

signal.signal(signal.SIGUSR1,handler)

pidPadre = os.getpid()

pid = os.fork()

if pid == 0:
    #signal.signal(signal.SIGUSR1,handler)
    hijo1()

padre()
