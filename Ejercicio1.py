import getopt
import sys

def suma(x,y):
    return x + y

def resta(x,y):
    return x - y

def multiplicacion(x,y):
    return x * y

def division(x,y):
    return x/y

num1 = 0
num2 = 0
operador = ""


(opt,arg) = getopt.getopt(sys.argv[1:], 'o:n:m:')

for (op,ar) in opt:
    if op == '-o':
        operador = ar
        str(operador)
    elif op == '-n':
        num1 = ar
        num1 = int(num1)
    elif op == '-m':
        num2 = ar
        num2 = int(num2)
    else:
        print("Argumento invalido")

if operador == '+':
    print("%d %c %d = %d" %(num1,operador,num2,(suma(num1,num2))))
elif operador == '-':
    print("%d %c %d = %d" %(num1,operador,num2,(resta(num1,num2))))
elif operador == '*':
    print("%d %c %d = %d" %(num1,operador,num2,(multiplicacion(num1,num2))))
elif operador == '/':
    print("%d %c %d = %d" %(num1,operador,num2,(division(num1,num2))))















