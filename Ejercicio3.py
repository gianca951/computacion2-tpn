"""
Escribir un programa que en ejecución genere dos procesos, uno padre y otro hijo. El
hijo debe escribir "Soy el hijo" 5 veces. El padre debe escribir "Soy el padre" 2 veces.
El padre debe esperar a que termine el hijo y mostrar el mensaje "Mi proceso hijo
termino".

"""

import os
import time

def hijo():
    for x in range(5):
        print("Soy el hijo y mi PID es %d" % os.getpid())

def padre():
    for x in range(2):
        print("Soy el padre y mi PID es %d" % os.getpid())
    time.sleep(1)
    print("Mi proceso hijo termino")
    

pid = os.fork()

if pid == 0:
    hijo()
else:
    padre()


