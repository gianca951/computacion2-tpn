import signal, os
from time import sleep

def handler(s,f):
    print("Señal de mi padre recibida!!!\n")

def hijo():
    while True:
        print("Esperando...")
        signal.pause()


signal.signal(signal.SIGUSR1,handler)
pid = os.fork()

if pid == 0:
    hijo()
else:
    while True:
        os.kill(pid,signal.SIGUSR1)
        sleep(5)


    

