"""
Escribir un programa en C que en ejecución genere 2 procesos (padre e hijo). Este
programa recibirá como único argumento del intérprete de órdenes un entero. El padre
incrementará ese valor en 4 y lo mostrará por pantalla. El hijo decrementará ese valor
en 2 y lo mostrará en pantalla.

"""

import os
import getopt
import sys

def child(x):
    result = x - 2
    print("El valor decrementado por el hijo es: %d" % result)

def parent(y):
    result = y + 4 
    print("El valor incrementado por el padre es: %d" % result)


num = ""

(opt,arg) = getopt.getopt(sys.argv[1:], 'n:')

for (op,ar) in opt:
	if(op == '-n'):
            num = ar
            num = int(num)


print("El numero ingresado como argumento es: %d" % num)

pid = os.fork()

if pid == 0:
    child(num)
else:
    parent(num)
