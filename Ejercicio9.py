import os, signal
from time import sleep

def handler(s,f):
    print("...recibi la señal numero %d" % s)                                 

def hijo(pid):
    print("\nSoy el PID %d, de mi padre PID %d..." % (os.getpid(),pid))
    signal.pause()
    os._exit(0)

pidPadre = os.getpid()

signal.signal(signal.SIGUSR1,handler)

for x in range(3):
    pid = os.fork()
    if pid == 0:
        hijo(pidPadre)
    else:
        i = 0
        while True:
            os.kill(pid,signal.SIGUSR1)
            sleep(1)
            if i == 1:
                break
            i += 1
    print("\n-----------------------------------------")
    sleep(2)
