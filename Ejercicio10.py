import os, time, sys, signal

def handler(s,f):
    print("\n...enviando señal...")

def B(w):
    signal.pause()
    os.close(r)
    w = os.fdopen(w, 'w')
    print("Proceso B escribiendo en la tuberia...")
    w.write("Mensaje 1 (PID=%d)\n" % os.getpid())
    w.close()

def C(w):
    signal.pause()
    os.close(r)
    w = os.fdopen(w, 'w')
    print("Proceso C escribiendo en la tuberia...")
    w.write("Mensaje 2 (PID=%d)\n" % os.getpid())
    w.close()

def A(r):
    os.close(w)
    r = os.fdopen(r)
    print("A (PID=%d) leyendo de la tuberia:" % os.getpid())
    linea = r.readline()
    while linea:
        print(linea[:-1]) #con [:-1] borro los saltos de linea
        linea = r.readline()
    r.close()
    print("\n")
    sys.exit(0)

signal.signal(signal.SIGUSR1, handler)

pidPadre = os.getpid()

r, w = os.pipe()

pid = os.fork()

if pid == 0:
    pid2 = os.fork()
    if pid2 == 0:
        C(w)
        time.sleep(1)
        os.kill(pidPadre, signal.SIGUSR1)
    else:
        B(w)
        time.sleep(1)
        os.kill(pid2, signal.SIGUSR1)
else:
    i = 0
    while True:
        os.kill(pid, signal.SIGUSR1)
        if i == 1:
            break
        i += 1
        time.sleep(1)
    signal.pause()
    A(r)
