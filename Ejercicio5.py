"""
Realice otro programa que genere la siguiente configuración de procesos, y cada
proceso al iniciar deberá mostrar “Soy el proceso N, mi padre es M” N será el PID de
cada hijo, y M el PID del padre.

"""

import os


def child():
   os._exit(0)

def parent():
   for x in range(3):
      pid = os.fork()
      if pid == 0:
         child()
      else:
         pids = (pid, os.getpid())
         print("Soy el proceso %d,mi padre es %d\n" % pids)

parent()
