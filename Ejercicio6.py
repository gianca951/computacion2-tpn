import signal
from time import sleep

def handler(s,f):
    print("Esta vez me saldre, intentalo de nuevo")
    #SIG_DFL setea la señal SIGINT a como estaba por defecto
    signal.signal(signal.SIGINT,signal.SIG_DFL)

signal.signal(signal.SIGINT,handler)

sleep(10)
